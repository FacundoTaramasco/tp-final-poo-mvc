package repositorio;

import modelo.ModeloApp;
import modelo.Producto;
import modelo.SimpleDataIO;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by spirok on 16/03/15.
 */
public class RepositorioProductoSQL implements RepositorioApp {

    private Connection conexionSQL;
    private ResultSet rs;

    private ModeloApp modelo;
    private ArrayList<Producto> listaProductos;

    public RepositorioProductoSQL(Connection conexionSQL, ModeloApp modelo) {
    	this.modelo = modelo;
    		//ArrayList<Producto> listaProductos) {
        this.listaProductos = this.modelo.getListaProductos();
        this.conexionSQL = conexionSQL;
    }

    @Override
    public boolean initRep() {
        String customQuery = "SELECT id, descripcion, cantidad, precioUnitario FROM Producto";
        try {
            rs  = SimpleDataIO.getResultSet(conexionSQL, customQuery);
            while (rs.next()) {
                listaProductos.add( new Producto(rs.getInt("id"),
                        rs.getString("descripcion").trim(),
                        rs.getInt("cantidad"),
                        rs.getDouble("precioUnitario")) );
            }
            return true;
        } catch (SQLException sqle) {
            SimpleDataIO.showSQLException(sqle);
            return false;
        }
    }

    @Override
    public boolean agregarElemento(Object obj) {
        if (!(obj instanceof Producto))
            return false;
        Producto nuevoProducto = (Producto)obj;
        Producto productoExistente;
        String queryInsert;
        // armo query de consulta para chequear si ya existe un producto con la id ingresada
        String queryAux = "SELECT id, descripcion, cantidad, precioUnitario " +
                "FROM Producto WHERE id = " + nuevoProducto.getId();
        ResultSet data = SimpleDataIO.getResultSet(this.conexionSQL, queryAux);
        try {
            if (data.next()) {
                productoExistente = new Producto(data.getInt("id"),
                        data.getString("descripcion").trim(),
                        data.getInt("cantidad"),
                        data.getDouble("precioUnitario"));
                modelo.mensajePorTerminal("Producto Existente : \n " + productoExistente);
                modelo.mensajePorGUI("Producto Existente : \n " + productoExistente);
                //Util.muestroError("Producto Existente : \n " + productoExistente);
                return false;
            }
        } catch (SQLException sqle) {
            SimpleDataIO.showSQLException(sqle);
            return false;
        }
        // ID disponible, armo query de insert
        queryInsert = "INSERT INTO Producto (id, descripcion, cantidad, precioUnitario) values (" +
                nuevoProducto.getId() + ", '" +
                nuevoProducto.getDescripcion()  + "', " +
                nuevoProducto.getCantidad()  + ", " +
                nuevoProducto.getPrecioUnitario()  + ")";
        if (SimpleDataIO.executeQuery(this.conexionSQL, queryInsert)) {
            SimpleDataIO.doCommit(this.conexionSQL);
            return true;
        } else
            return false;

    }

    @Override
    public boolean eliminarElemento(Object obj) {
        if (!(obj instanceof Producto))
            return false;
        Producto productoEliminar = (Producto)obj;
        String query = "DELETE FROM Producto WHERE id = " + productoEliminar.getId();
        if (SimpleDataIO.executeQuery(conexionSQL, query)) {
            SimpleDataIO.doCommit(conexionSQL);
            return true;
        } else
            return false;
    }

    @Override
    public boolean modificarElemento(Object anterior, Object nuevo) {
        if (!(anterior instanceof Producto))
            return false;
        if (!(nuevo instanceof Producto))
            return false;
        Producto productoNuevo = (Producto)nuevo;
        Producto productoAnterior = (Producto)anterior;
        Producto productoExistente;
        String queryUpdate;
        String queryAux = "SELECT id, descripcion, cantidad, precioUnitario " +
                "FROM Producto WHERE id = " + productoNuevo.getId() + " AND id <> " + productoAnterior.getId();
        ResultSet data = SimpleDataIO.getResultSet(conexionSQL, queryAux);
        try {
            if (data.next()) {
                productoExistente = new Producto(data.getInt("id"),
                        data.getString("descripcion").trim(),
                        data.getInt("cantidad"),
                        data.getDouble("precioUnitario"));
                return false;
            }
        } catch (SQLException sqle) {
            SimpleDataIO.showSQLException(sqle);
            return false;
        }
        queryUpdate = "UPDATE Producto SET" +
                " id = " + productoNuevo.getId() + ", " +
                " descripcion = '" + productoNuevo.getDescripcion() +"', " +
                " cantidad = " + productoNuevo.getCantidad() +  ", " +
                " precioUnitario = " + productoNuevo.getPrecioUnitario() +
                " WHERE id = " + productoAnterior.getId();

        if (!SimpleDataIO.executeQuery(conexionSQL, queryUpdate))
            return false;
        SimpleDataIO.doCommit(conexionSQL);
        return true;
    }

    @Override
    public void cerrarFlujos() {
    	SimpleDataIO.finish(conexionSQL);
    }
}
