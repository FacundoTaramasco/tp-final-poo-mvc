package repositorio;


/**
 * Created by spirok on 16/03/15.
 */
public interface RepositorioApp {

    boolean initRep();

    boolean agregarElemento(Object obj);
    boolean eliminarElemento(Object obj);
    boolean modificarElemento(Object anterior, Object nuevo);

    void cerrarFlujos();
}
