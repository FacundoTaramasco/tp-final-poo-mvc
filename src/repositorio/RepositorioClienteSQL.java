package repositorio;

import modelo.Cliente;
import modelo.SimpleDataIO;

import java.sql.SQLException;
import java.util.*;

import java.sql.Connection;
import java.sql.ResultSet;

/**
 * Created by Spirok on 25/03/2015.
 */
public class RepositorioClienteSQL implements RepositorioApp {

    private Connection conexionSQL = null;
    private ResultSet rs           = null;

    private ArrayList<Cliente> listaClientes;
    /**
     * Constructor
     */
    public RepositorioClienteSQL(Connection conexionSQL, ArrayList<Cliente> listaClientes) {
        this.conexionSQL = conexionSQL;
        this.listaClientes = listaClientes;
    }

    @Override
    public boolean initRep() {
        String customQuery = "SELECT cuit, apellido, nombre, direccion, telefono FROM Cliente";
        try {
            rs  = SimpleDataIO.getResultSet(conexionSQL, customQuery);
            while (rs.next()) {
                listaClientes.add(new Cliente(rs.getString("cuit").trim(),
                        rs.getString("apellido").trim(),
                        rs.getString("nombre").trim(),
                        rs.getString("direccion").trim(),
                        rs.getString("telefono").trim()));
            }
        } catch (SQLException sqle) {
            SimpleDataIO.showSQLException(sqle);
        }
        return false;
    }

    @Override
    public boolean agregarElemento(Object obj) {
        return false;
    }

    @Override
    public boolean eliminarElemento(Object obj) {
        return false;
    }

    @Override
    public boolean modificarElemento(Object anterior, Object nuevo) {
        return false;
    }

    @Override
    public void cerrarFlujos() {
    	SimpleDataIO.finish(conexionSQL);
    }
}
