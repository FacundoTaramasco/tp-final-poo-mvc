package controlador;

import modelo.ModeloApp;
import modelo.Producto;
import utileria.Util;
import vista.AgregarProducto;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by spirok on 16/03/15.
 */
public class CtrAgregarProducto implements ActionListener {

    private ModeloApp modelo;
    private AgregarProducto vistaAgregar;

    /**
     * Constructor
     */
    public CtrAgregarProducto(ModeloApp m, AgregarProducto vista) {
        this.modelo = m;
        this.vistaAgregar = vista;
        vistaAgregar.getBtnAceptar().addActionListener(this);
        vistaAgregar.getBtnCancelar().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaAgregar.getBtnAceptar()) {
            Producto nuevoProducto = new Producto();
            try {
                int idp      = Integer.parseInt(vistaAgregar.getTxtId().getText());
                String descp = vistaAgregar.getTxtDescripcion().getText();
                int cantp    = Integer.parseInt(vistaAgregar.getTxtCantidad().getText());
                double precp = Double.parseDouble(vistaAgregar.getTxtPrecioUnitario().getText());
                if (!nuevoProducto.setId(idp)) {
                    Util.muestroError("Error Id", vistaAgregar.getTxtId()); return;
                }
                if (!nuevoProducto.setDescripcion(descp)) {
                    Util.muestroError("Error en Descripcion", vistaAgregar.getTxtDescripcion()); return;
                }
                if (!nuevoProducto.setCantidad(cantp)) {
                    Util.muestroError("Error en Cantidad", vistaAgregar.getTxtCantidad()); return;
                }
                if (!nuevoProducto.setPrecioUnitario(precp)) {
                    Util.muestroError("Error en Precio Unitario", vistaAgregar.getTxtPrecioUnitario()); return; }
            } catch (NumberFormatException nef) {
                Util.muestroError("Valores incorrectos");
                return;
            }

            if (modelo.agregarProducto(nuevoProducto)) {
                vistaAgregar.setearCampos();
                Util.muestroMensaje("Producto Agregado");
            }

        }
        if (e.getSource() == vistaAgregar.getBtnCancelar()) {
            vistaAgregar.dispose();
        }
    }
}
