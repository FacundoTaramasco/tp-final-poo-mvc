package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.beans.PropertyVetoException;

import modelo.ModeloApp;
import modelo.Producto;
import utileria.Util;
import vista.AgregarProducto;
import vista.GUIMain;

/**
 * Created by spirok on 16/03/15.
 */
public class CtrMain implements ActionListener {

    private ModeloApp modelo;
    private GUIMain vista;

    private JTable tablaProductos;
    private JTable tablaClientes;
    
    private JButton btnAgregarProducto;
    private JButton btnEliminarProducto;
    
    /**
     * Constructor
     */
    public CtrMain(ModeloApp m, GUIMain v) {
        this.modelo = m;
        this.vista = v;
        // establece modelo de la jtable
        tablaProductos = vista.getPanelP().getTablaProducto();
        tablaClientes  = vista.getPanelC().getTablaClientes();

        btnAgregarProducto  = vista.getPanelP().getBtnAgregar();
        btnEliminarProducto = vista.getPanelP().getBtnEliminar();
        		
        tablaProductos.setModel( modelo.getModeloTablaProducto() );
        tablaClientes.setModel( modelo.getModeloTablaCliente() );

        btnAgregarProducto.addActionListener(this);
        btnEliminarProducto.addActionListener(this);
        
        vista.getItmProd().addActionListener(this);
        vista.getItmCli().addActionListener(this);
        vista.getItmCerrar().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        /** click boton agregar producto **/
        if (e.getSource() == btnAgregarProducto) {
            AgregarProducto vistaAgregar = new AgregarProducto();
            new CtrAgregarProducto(modelo, vistaAgregar);
        }
        /** click boton eliminar producto **/
        if (e.getSource() == btnEliminarProducto) {
            int indice = tablaProductos.getSelectedRow();
            if ( indice != -1) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                Producto producoEliminar = modelo.getModeloTablaProducto().getListaProductos().get(indice);
                int dialogResult = JOptionPane.showConfirmDialog(null, "¿Desea Eliminar el Producto?\n" +
                        producoEliminar, "Eliminar Producto", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    if (modelo.eliminarProducto(producoEliminar))
                        Util.muestroMensaje("Producto Eliminado");
                    else
                        Util.muestroError("Error al eliminar.");
                }
            }
        }
        /** click menuitem producto **/
        if (e.getSource() == vista.getItmProd()) {
            try {
                vista.getPanelP().setIcon(false);
                vista.getPanelP().toFront();
                vista.getPanelP().setSelected(true);
            } catch (PropertyVetoException pve) {
                return;
            }
        }
        /** click menuitem cliente **/
        if (e.getSource() == vista.getItmCli()) {
            try {
                vista.getPanelC().setIcon(false);
                vista.getPanelC().toFront();
                vista.getPanelC().setSelected(true);
            } catch (PropertyVetoException pve) {
                return;
            }
        }
        /** click menuitem cerrar **/
        if (e.getSource() == vista.getItmCerrar()) {
            modelo.finApp();
            System.exit(0);
        }
    }
}
