package vista;

import javax.swing.*;
import java.awt.*;

import utileria.Util;

/**
 * Created by spirok on 16/03/15.
 */
public class PanelProductos extends JInternalFrame  {

    private JButton btnAgregar;
    private JButton btnEliminar;

    public JTable tablaProducto;

    private GridBagConstraints gbc = new GridBagConstraints();

    /**
     * Constructor
     */
    public PanelProductos() {
        super("Productos", true, false, true, true);

        this.setLayout( new GridBagLayout() );

        btnAgregar    = new JButton("Agregar");
        btnEliminar   = new JButton("Eliminar");

        tablaProducto = new JTable();
        tablaProducto.setToolTipText("Doble click en las celdas para modificar datos.");
        tablaProducto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JPanel panelBotones = new JPanel( new GridBagLayout() );
        //panelBotones.setBorder(BorderFactory.createLineBorder(Color.black));

        Util.agregarWidgetJC( panelBotones, gbc, btnAgregar        , 0, 0, 1, 0, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelBotones, gbc, btnEliminar       , 1, 0, 1, 0, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelBotones, gbc, new JLabel("")    , 2, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetJC( this, gbc, panelBotones                   , 0, 0, 1, 1, 0, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( this, gbc, new JScrollPane(tablaProducto) , 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH );

    }

    /**
     * Metodo que establece el estado de algunos widgets.
     * @param estado boolean del estado a setear a los widgets.
     */
    public void estadoComponentes(boolean estado) {
        btnAgregar.setEnabled(estado);
        btnEliminar.setEnabled(estado);
        tablaProducto.setEnabled(estado);
    }

    public JButton getBtnAgregar() {
        return btnAgregar;
    }

    public JButton getBtnEliminar() {
        return btnEliminar;
    }

    public JTable getTablaProducto() {
        return this.tablaProducto;
    }
}
