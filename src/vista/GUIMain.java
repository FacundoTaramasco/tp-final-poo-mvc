package vista;

import javax.swing.*;
import java.awt.*;

/**
 * Created by spirok on 16/03/15.
 */
@SuppressWarnings("serial")
public class GUIMain extends JFrame {

    private JMenuBar barra;
    private JMenu optBarra;
    private JMenuItem itmProd;
    private JMenuItem itmCli;
    private JMenuItem itmFac;
    private JMenuItem itmCerrar;

    private JDesktopPane JDP;

    private PanelProductos PanelP = null;
    private PanelClientes PanelC    = null;
    /*
        private PanelFacturacion PanelF = null;
        */


    /**
     * Constructor
     */
    public GUIMain() {
        super("TP-Final-POO");

        this.getContentPane().setLayout(new BorderLayout());

        barra     = new JMenuBar();
        optBarra  = new JMenu("Opciones");
        itmProd   = new JMenuItem("Productos");
        itmCli    = new JMenuItem("Clientes");
        itmFac    = new JMenuItem("Facturacion");
        itmCerrar = new JMenuItem("Cerrar");

        optBarra.add(itmProd);
        optBarra.add(itmCli);
        optBarra.add(itmFac);
        optBarra.add(itmCerrar);
        barra.add(optBarra);

        this.setJMenuBar(barra);

        // crear y establecer JDesktopPane
        JDP = new JDesktopPane();
        //JDP.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);

        this.add(JDP, "Center");
        this.add(new JLabel("  "), "South"); //bottom bar de prueba!

        PanelP = new PanelProductos();
        PanelC = new PanelClientes();
        /*
            PanelF   = new PanelFacturacion(cnxBBDD, PanelP, PanelC);
            */

        // adjuntar marcos internos al JDesktopPane y mostrarlos
        JDP.add(PanelP);
        PanelP.setSize(550, 320);
        PanelP.setLocation(10, 10);
        PanelP.setVisible(true);

        JDP.add(PanelC);
        PanelC.setSize(550, 320);
        PanelC.setLocation(10, 335);
        PanelC.setVisible(true);
            /*
            JDP.add(PanelF);
            PanelF.setSize(790, 645);
            PanelF.setLocation(565, 10);
            PanelF.setVisible(true);
            */
        this.setSize(1250, 680);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setVisible(true);
    }

    public PanelProductos getPanelP() { return PanelP; }

    public PanelClientes getPanelC() { return PanelC; }

    public JMenuItem getItmProd() { return itmProd; }

    public JMenuItem getItmCli() { return itmCli; }

    public JMenuItem getItmFac() { return itmFac; }

    public JMenuItem getItmCerrar() {  return itmCerrar; }
}