package vista;

import javax.swing.*;
import java.awt.*;

import utileria.Util;

/**
 * Created by spirok on 16/03/15.
 */
public class AgregarProducto extends JFrame  {

    private JTextField txtId;
    private JTextField txtDescripcion;
    private JTextField txtCantidad;
    private JTextField txtPrecioUnitario;

    private JButton btnAceptar;
    private JButton btnCancelar;

    private GridBagConstraints gbc   = new GridBagConstraints();

    /**
     * Constructor
     */
    public AgregarProducto() {

        this.getContentPane().setLayout(new GridBagLayout() );

        txtId             = new JTextField();
        txtDescripcion    = new JTextField();
        txtCantidad       = new JTextField();
        txtPrecioUnitario = new JTextField();

        btnAceptar        = new JButton("Aceptar");
        btnCancelar       = new JButton("Cancelar");

        Util.agregarWidgetCT( this, gbc, new JLabel("ID : ")              , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtId                            , 1, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Descripcion : ")     , 0, 1, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtDescripcion                   , 1, 1, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Cantidad : ")        , 0, 2, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtCantidad                      , 1, 2, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Precio Unitario : ") , 0, 3, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtPrecioUnitario                , 1, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetCT( this, gbc, btnAceptar                       , 0, 4, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, btnCancelar                      , 1, 4, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("")                   , 0, 5, 2, 1, 1, 1, GridBagConstraints.BOTH );

        this.setSize(340, 170);
        this.setLocationRelativeTo(this);
        this.setTitle("Agregar Producto");

        this.setResizable(false);

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.setVisible(true);
    }

    public JTextField getTxtId() {
        return txtId;
    }

    public JTextField getTxtDescripcion() {
        return txtDescripcion;
    }

    public JTextField getTxtCantidad() {
        return txtCantidad;
    }

    public JTextField getTxtPrecioUnitario() {
        return txtPrecioUnitario;
    }

    public JButton getBtnAceptar() {
        return btnAceptar;
    }

    public JButton getBtnCancelar() {
        return btnCancelar;
    }


    public void setearCampos() {
        txtId.setText("");
        txtDescripcion.setText("");
        txtCantidad.setText("");
        txtPrecioUnitario.setText("");
    }

}
