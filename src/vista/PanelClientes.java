package vista;

import java.awt.*;
import javax.swing.*;


import utileria.Util;

/**
 * Created by spirok on 20/03/15.
 */
public class PanelClientes  extends JInternalFrame {

    private JButton btnAgregar;
    private JButton btnEliminar;

    private JTable tablaClientes;

    private GridBagConstraints gbc  = new GridBagConstraints();


    /**
     * Constructor
     */
    public PanelClientes(){
        super("Clientes", true, false, true, true);


        this.setLayout( new GridBagLayout() );

        btnAgregar    = new JButton("Agregar");
        btnEliminar   = new JButton("Eliminar");

        // creando jtable con el modelo de Clientes
        tablaClientes = new JTable();
        tablaClientes.setToolTipText("Doble click en las celdas para modificar datos.");
        tablaClientes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);;

        Util.agregarWidgetJC( this, gbc, btnAgregar     , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( this, gbc, btnEliminar    , 1, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( this, gbc, new JLabel("") , 2, 0, 1, 1, 1, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( this, gbc, new JScrollPane(tablaClientes), 0, 1, 4, 1, 1, 1, GridBagConstraints.BOTH );
    }

    public JTable getTablaClientes() {
        return this.tablaClientes;
    }
    
    /**
     * Metodo que establece el estado de algunos widgets.
     */
    public void estadoComponentes(boolean estado) {
        btnAgregar.setEnabled(estado);
        btnEliminar.setEnabled(estado);
        tablaClientes.setEnabled(estado);
    }

}
