package modelo;

import repositorio.RepositorioApp;
import repositorio.RepositorioClienteSQL;
import repositorio.RepositorioProductoSQL;

import java.sql.Connection;
import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * Created by spirok on 16/03/15.
 */
public class ModeloApp {

    private RepositorioApp repProd;
    private RepositorioApp repCli;

    private ArrayList<Producto> listaProductos;
    private ArrayList<Cliente> listaClientes;

    private ModeloTablaProductos MTP;
    private ModeloTablaClientes MTC;

    /**
     * Constructor
     */
    public ModeloApp() {
        listaProductos = new ArrayList<Producto>();
        listaClientes  = new ArrayList<Cliente>();

        MTP = new ModeloTablaProductos(this);
        MTC = new ModeloTablaClientes(this);

        final String urlPostgres   = "jdbc:postgresql://localhost:5432/TP-Final-POO";
        final String userPostgres  = "postgres";
        final String psswdPostgres = "masterkey";
        
        Connection conexionPostgres = SimpleDataIO.getConnection(urlPostgres, 
        		                                                 userPostgres, 
        		                                                 psswdPostgres);

        repProd = new RepositorioProductoSQL(conexionPostgres, this);
        repProd.initRep();

        repCli = new RepositorioClienteSQL(conexionPostgres, listaClientes);
        repCli.initRep();
    }

    /**
     * Metodo que agrega un producto al sistema.
     * @param prod nuevo Producto.
     * @return boolean true si agrego correctamente, false caso contrario.
     */
    public boolean agregarProducto(Producto prod) {
        if (!repProd.agregarElemento(prod)) {
            return false;
        }
        getListaProductos().add(prod);
        getModeloTablaProducto().udpdateProductos();
        return true;
    }

    /**
     * Metodo que elimina un producto del sistema.
     * @param prod Producto a dar de baja.
     * @return boolean true si elimino correctamente, false caso contrario.
     */
    public boolean eliminarProducto(Producto prod) {
        if (!repProd.eliminarElemento(prod)) {
            return false;
        }
        int indice = getListaProductos().indexOf(prod);
        getListaProductos().remove(indice);
        getModeloTablaProducto().udpdateProductos();
        return true;
    }

    /**
     * Metodo que modifica un producto del sistema.
     * @param anterior Producto al que se desea modificar.
     * @param nuevo Producto seteado con los nuevos valores.
     * @return boolean true si modifico correctamente, false caso contrario.
     */
    public boolean modificarProducto(Producto anterior, Producto nuevo) {
        if (!repProd.modificarElemento(anterior, nuevo)) {
            return false;
        }
        int indice = getListaProductos().indexOf(anterior);
        getListaProductos().set(indice, nuevo);
        return true;
    }

    public boolean agregarCliente(Cliente nuevoCli) {
        return true;
    }

    public ArrayList<Producto> getListaProductos() { return listaProductos; }

    public ArrayList<Cliente> getListaClientes() { return listaClientes; }

    public ModeloTablaProductos getModeloTablaProducto() { return this.MTP; }

    public ModeloTablaClientes getModeloTablaCliente() { return this.MTC; }

    public void mensajePorTerminal(String msj) {
    	System.out.println(msj);
    }
    
    public void mensajePorGUI(String msj) {
    	JOptionPane.showMessageDialog(null, msj);
    }
    
    public void finApp() {
        repProd.cerrarFlujos();
        repCli.cerrarFlujos();
    }

}
