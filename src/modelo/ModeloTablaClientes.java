package modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 * Created by Spirok on 20/03/2015.
 */
@SuppressWarnings("serial")
public class ModeloTablaClientes  extends AbstractTableModel {

    private ModeloApp modelo;
    private ArrayList<Cliente> listaClientes = null;

    /**
     * Constructor
     */
    public ModeloTablaClientes(ModeloApp modelo) {
        this.modelo = modelo;
        this.listaClientes = this.modelo.getListaClientes();
    }

    // --------------------------------------------- Metodos obligatorios --------------------------------------------------

    @Override
    public Object getValueAt(int fila, int columna) {
        switch(columna) {
            case 0 : return getListaClientes().get(fila).getCuit();
            case 1 : return getListaClientes().get(fila).getApellido();
            case 2 : return getListaClientes().get(fila).getNombre();
            case 3 : return getListaClientes().get(fila).getDireccion();
            case 4 : return getListaClientes().get(fila).getTelefono();
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return getListaClientes().size();
    }

    // ---------------------------------------------- Metodos adicionales -----------------------------------------------------

    @Override
    public boolean isCellEditable(int fila, int columna) {
        return true;
    }

    @Override
    public String getColumnName(int columna) {
        switch(columna) {
            case 0 : return "CUIT";
            case 1 : return "Apellido";
            case 2 : return "Nombre";
            case 3 : return "Direccion";
            case 4 : return "Telefono";
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columna) {
        switch(columna) {
            case 0 : return String.class;  // cuit
            case 1 : return String.class;  // apellido
            case 2 : return String.class;  // nombre
            case 3 : return String.class;  // direccion
            case 4 : return String.class;  // telefono
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int fila, int columna) {

    }

    /**
     * Metodo que retorna el ArrayList con todos los Clientes.
     * @return listaClientes ArrayList<Cliente>
     */
    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }

}

