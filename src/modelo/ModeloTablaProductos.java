package modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 * Created by Spirok on 17/03/2015.
 */
@SuppressWarnings("serial")
public class ModeloTablaProductos extends AbstractTableModel {

    private ModeloApp modelo;
    private ArrayList<Producto> listaProductos = null;

    /**
     * Constructor
     * @param modelo capa modelo de la app.
     */
    public ModeloTablaProductos(ModeloApp modelo) {
        this.modelo = modelo;
        this.listaProductos = modelo.getListaProductos();
    }


    // --------------------------------------------- Metodos obligatorios --------------------------------------------------

    @Override
    public Object getValueAt(int fila, int columna) {
        switch(columna) {
            case 0 : return getListaProductos().get(fila).getId();
            case 1 : return getListaProductos().get(fila).getDescripcion();
            case 2 : return getListaProductos().get(fila).getCantidad();
            case 3 : return getListaProductos().get(fila).getPrecioUnitario();
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        return listaProductos.size();
    }

    // ---------------------------------------------- Metodos adicionales -----------------------------------------------------

    @Override
    public boolean isCellEditable(int fila, int columna) {
        return true;
    }

    @Override
    public String getColumnName(int columna) {
        switch(columna) {
            case 0 : return "ID";
            case 1 : return "Descripcion";
            case 2 : return "Cantidad";
            case 3 : return "Precio Unitario";
        }
        return null;
    }

	@Override
    public Class<?> getColumnClass(int columna) {
        switch(columna) {
            case 0 : return Integer.class; // id
            case 1 : return String.class;  // descripcion
            case 2 : return Integer.class; // cantidad
            case 3 : return Double.class;  // precio unitario
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int fila, int columna) {
        Producto productoAnterior = new Producto(getListaProductos().get(fila).getId(),
                getListaProductos().get(fila).getDescripcion(),
                getListaProductos().get(fila).getCantidad(),
                getListaProductos().get(fila).getPrecioUnitario());
        Producto productoModificado = new Producto(getListaProductos().get(fila).getId(),
                getListaProductos().get(fila).getDescripcion(),
                getListaProductos().get(fila).getCantidad(),
                getListaProductos().get(fila).getPrecioUnitario());
        if (valoresOk(aValue, columna, productoModificado)) {
            this.modelo.modificarProducto(productoAnterior, productoModificado);
        }
    }


    /**
     * Metodo que retorna el ArrayList con todos los Productos.
     * @return listaProductos ArrayList<Producto>
     */
    public ArrayList<Producto> getListaProductos() {
        return listaProductos;
    }

    public void udpdateProductos() {
        this.fireTableDataChanged();
    }


    private boolean valoresOk(Object value, int columna, Producto productoModificado) {
        switch (columna) {
            case 0:
                if (!productoModificado.setId((Integer) value))
                    return false;
                break;
            case 1:
                if (!productoModificado.setDescripcion((String) value))
                    return false;
                break;
            case 2:
                if (!productoModificado.setCantidad((Integer) value))
                    return false;
                break;
            case 3:
                if (!productoModificado.setPrecioUnitario((Double) value))
                    return false;
        }
        return true;
    }
}
