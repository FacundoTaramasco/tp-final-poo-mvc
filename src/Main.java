import controlador.CtrMain;
import modelo.ModeloApp;
import vista.GUIMain;

import javax.swing.*;

/**
 * Created by spirok on 16/03/15.
 */
public class Main {

    public static void main(String[] args) {

        // jugando con la apariencia de swing
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

        ModeloApp model = new ModeloApp();
        GUIMain view = new GUIMain();
        CtrMain controller = new CtrMain(model, view);

    }
}
